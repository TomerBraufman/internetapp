﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using internetApp.Models;

namespace internetApp.Filters
{
    public class RequireLoginFilter : ActionFilterAttribute, IAuthenticationFilter
    {
        public bool AdminRequired { get; set; }

        public RequireLoginFilter() { AdminRequired = false; }

        public void OnAuthentication(AuthenticationContext filterContext)
        {
            var db = new Context();
            var user = filterContext.HttpContext.Session["USER"] as User;

            if (user == null)
            {
                filterContext.Result = new RedirectResult("/Users/Login");
            }
            else
            {
                filterContext.HttpContext.Session["USER"] = db.Users.Find(user.Id);

                if (AdminRequired && !user.IsAdmin)
                {
                    filterContext.Result = new HttpUnauthorizedResult();
                }
            }

            db.Dispose();
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {

        }
    }
}