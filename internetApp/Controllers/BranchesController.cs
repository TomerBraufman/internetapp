﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using internetApp;
using internetApp.Filters;
using internetApp.Models;
using internetApp.ViewModels;

namespace internetApp.Controllers
{
    public class BranchesController : Controller
    {
        private Context db = new Context();

        // GET: Branches
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Index(BranchSearchViewModel search)
        {
            ICollection<Branch> branches = db.Branches.Where(branch => search.Name == null || branch.Name.Contains(search.Name))
                .Where(branch => search.Supervisor == null || branch.Supervisor.FullName.Contains(search.Supervisor)).ToList();
            return View(branches);
        }

        // GET: Branches/Details/5
        public ActionResult Map()
        {
            return View(db.Branches.ToList());
        }

        // GET: Branches/Details/5
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Branch branch = db.Branches.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            return View(branch);
        }

        // GET: Branches/Create
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Create()
        {
            var allUsers = db.Users.Where(user => user.IsAdmin);
            this.ViewBag.usersSelectable = (IEnumerable<Models.User>)allUsers;
            return View();
        }
        // POST: Branches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [RequireLoginFilter(AdminRequired = true)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Latitude,Longtitude,SupervisorId")] Branch branch)
        {
            if (ModelState.IsValid)
            {
                branch.Id = Guid.NewGuid();
                db.Branches.Add(branch);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            var allUsers = db.Users.Where(user => user.IsAdmin);
            this.ViewBag.usersSelectable = (IEnumerable<Models.User>)allUsers;
            return View(branch);
        }

        // GET: Branches/Edit/5
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Branch branch = db.Branches.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            var allUsers = db.Users.Where(user => user.IsAdmin);
            this.ViewBag.usersSelectable = (IEnumerable<Models.User>)allUsers;
            return View(branch);
        }

        // POST: Branches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [RequireLoginFilter(AdminRequired = true)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Latitude,Longtitude,supervisorId")] Branch branch)
        {
            if (ModelState.IsValid)
            {
                db.Entry(branch).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var allUsers = db.Users.Where(user => user.IsAdmin);
            this.ViewBag.usersSelectable = (IEnumerable<Models.User>)allUsers;
            return View(branch);
        }

        // GET: Branches/Delete/5
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Branch branch = db.Branches.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            return View(branch);
        }

        // POST: Branches/Delete/5
        [RequireLoginFilter(AdminRequired = true)]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Branch branch = db.Branches.Find(id);
            db.Branches.Remove(branch);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
