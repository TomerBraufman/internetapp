﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using internetApp;
using internetApp.Filters;
using internetApp.Models;
using internetApp.ViewModels;

namespace internetApp.Controllers
{
    public class UsersController : Controller
    {
        private Context db = new Context();

        // GET: Users
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Index(UserSearchViewModel search)
        {
            ICollection<User> users = db.Users.Where(user => search.Email == null || user.Email.Contains(search.Email))
                .Where(user => search.FullName == null || user.FullName.Contains(search.FullName))
                .Where(user => search.Address == null || user.Address.Contains(search.Address)).ToList();
            return View(users);
        }

        // GET: Users/Details/5
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }
        // GET: Users/Logout
        public ActionResult Logout()
        {
            HttpContext.Session.Remove("USER");
            return RedirectToAction("Login");
        }

        // GET: Users/Login
        public ActionResult Login()
        {
            return View();
        }

        // POST: Users/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel loginUser)
        {
            User user = db.Users.FirstOrDefault(x => x.Email == loginUser.Email && x.Password == loginUser.Password);

            if (user != null)
            {
                HttpContext.Session.Add("USER", user);
                return RedirectToAction("Index", "Products");
            }

            return View(loginUser);
        }

        // GET: Users/SignUp
        public ActionResult SignUp()
        {
            return View();
        }

        // POST: Users/SignUp
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignUp([Bind(Include = "Email,Password,Address,FullName")] User user)
        {
            if (ModelState.IsValid)
            {
                user.Id = Guid.NewGuid();
                user.IsAdmin = false;
                db.Users.Add(user);
                db.SaveChanges();
                HttpContext.Session.Add("USER",user);
                return RedirectToAction("Index","Products");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [RequireLoginFilter(AdminRequired = true)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,Password,Address,IsAdmin,FullName")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [RequireLoginFilter(AdminRequired = true)]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
