﻿using internetApp.Models;
using internetApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace internetApp.Controllers
{
    public class ShoppingCartController : Controller
    {
        private Context db = new Context();
        // GET: ShoppingCart
        public ActionResult Index()
        {
            List<ProductOrder> shoppingCart = HttpContext.Session["ShoppingCart"] as List<ProductOrder>;
            ViewBag.Branches = db.Branches.ToList();
            if (HttpContext.Session["BranchIdToOrderFrom"] == null)
            {
                HttpContext.Session.Add("BranchIdToOrderFrom", ViewBag.Branches[0].Id);
            }
            return View(shoppingCart);
        }

        // Post: ShoppingCart/Add
        [HttpPost]
        public ActionResult Add(Guid prodId)
        {
            List<ProductOrder> shoppingCart = HttpContext.Session["ShoppingCart"] as List<ProductOrder>;

            if (shoppingCart == null) {
                HttpContext.Session.Add("ShoppingCart", new List<ProductOrder>());
            }
            List<Guid> shoppingCartIds = shoppingCart.Select(pO => pO.ProductId).ToList();
            if (!shoppingCartIds.Contains(prodId))
            {
                ProductOrder pO = new ProductOrder();
                pO.ProductId = prodId;
                pO.Product = db.Products.Find(prodId);
                pO.Quantity = 1;
                shoppingCart.Add(pO);
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        // Post: ShoppingCart/ChangeQuantity
        [HttpPost]
        public ActionResult ChangeQuantity(Guid prodId, int quantity)
        {
            List<ProductOrder> shoppingCart = HttpContext.Session["ShoppingCart"] as List<ProductOrder>;
            ProductOrder productOrderToUpdate = shoppingCart.Find(o => o.ProductId == prodId);
            productOrderToUpdate.Quantity = quantity;

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        // Post: ShoppingCart/ChangeBranch
        [HttpPost]
        public ActionResult ChangeBranch(Guid branchId)
        {
            HttpContext.Session["BranchIdToOrderFrom"] = branchId;
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        // GET: ShoppingCart/Order
        public ActionResult Order()
        {
            Order o = new Order();
            Product p;
            o.Id = Guid.NewGuid();
            o.UserId = (HttpContext.Session["USER"] as User).Id;
            if (HttpContext.Session["BranchIdToOrderFrom"] != null)
            {
                o.BranchId = (Guid)HttpContext.Session["BranchIdToOrderFrom"];
            }
            db.Orders.Add(o);
            db.SaveChanges();
            o = db.Orders.Where(or => or.Id == o.Id).SingleOrDefault();
            List<ProductOrder> pO = HttpContext.Session["ShoppingCart"] as List<ProductOrder>;
            pO.ForEach(pOItem => {
                p = db.Products.Where(pr => pr.Id == pOItem.ProductId).SingleOrDefault();
                pOItem.Order = o;
                pOItem.Product = p;
                db.ProductOrders.Add(pOItem);
            });
            // o.Products = db.Products.Where(p => pIds.Contains(p.Id)).ToList();
            //o.ProductOrders = pO;
            db.SaveChanges();
            ViewBag.branchIdToOrderFrom = null;
            HttpContext.Session.Add("ShoppingCart", new List<ProductOrder>());
            return RedirectToAction("MyOrders", "Orders");
        }
        // Post: ShoppingCart/Delete
        // TODO: Fix
        [HttpPost]
        public ActionResult Delete(Guid prodId)
        {
            (HttpContext.Session["ShoppingCart"] as List<ProductOrder>).RemoveAll(po => po.ProductId == prodId);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

    }
}
