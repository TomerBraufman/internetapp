﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using internetApp;
using internetApp.Filters;
using internetApp.Models;
using internetApp.ViewModels;

namespace internetApp.Controllers
{
    public class ProductsController : Controller
    {
        private Context db = new Context();

        // GET: Products
        public ActionResult Index(SearchViewModel search)
        {
            ICollection<Product> recommendations = null;
            if (Session["USER"] != null)
            {
                Guid id = (Session["USER"] as User).Id;
                IQueryable<Product> myProducts = db.Orders
                    .Where(order => order.UserId == id)
                    .Include(order => order.ProductOrders.Select(pO => pO.Product)).SelectMany(order => order.ProductOrders.Select(pO => pO.Product));
                recommendations =
                    myProducts.Include(product => product.ProductOrders.Select(pO => pO.Order))
                    .SelectMany(product => product.ProductOrders.Select(pO => pO.Order))
                    .Where(order => order.UserId != id)
                    .SelectMany(order => order.ProductOrders.Select(pO => pO.Product))
                    .Where(product => !myProducts.Contains(product)).ToList();
            }
            ICollection<Product> products = db.Products.Where(product => search.brand == null || product.Brand.Contains(search.brand))
                .Where(product => search.category == null || product.Category.Contains(search.category))
                .Where(product => search.color == null || product.Color.Contains(search.color)).ToList();
            return View(model :new CatalogViewModel(products, recommendations));
        }

        // GET: Products/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Create([Bind(Include = "Id,Name,Description,Category,Brand,Color,Price,Picture")] Product product)
        {
            if (ModelState.IsValid)
            {
                product.Id = Guid.NewGuid();
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(product);
        }

        // GET: Products/Edit/5
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [RequireLoginFilter(AdminRequired = true)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,Category,Brand,Color,Price,Picture")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(product);
        }
        // POST: Products/Delete
        [RequireLoginFilter(AdminRequired = true)]
        [HttpPost]
        public ActionResult Delete(Guid prodId)
        {
            Product product = db.Products.Find(prodId);
            db.Products.Remove(product);
            db.SaveChanges();
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
