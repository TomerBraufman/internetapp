﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using internetApp;
using internetApp.Filters;
using internetApp.Models;
using internetApp.ViewModels;



namespace internetApp.Controllers
{
    public class StatisticsController : Controller
    {
        private Context c = new Context();

        // GET: Statistics
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Index()
        {

            ViewData["data1"] = c.Products.GroupBy(p => p.Price)
                   .Select(g => new { price = g.Key, count = g.Count()}).ToList();

            ViewData["data2"] = c.Products.GroupBy(p => p.Category)
                  .Select(g => new { category = g.Key, count = g.Count() }).ToList();

            return View();
        }

       
    }
}
