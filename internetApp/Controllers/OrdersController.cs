﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using internetApp;
using internetApp.Filters;
using internetApp.Models;
using internetApp.ViewModels;

namespace internetApp.Controllers
{
    public class OrdersController : Controller
    {
        private Context db = new Context();

        // GET: Orders/Branch
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Branch()
        {
            Guid id = (Session["USER"] as User).Id;
            ICollection<Order> orders = db.Orders.Where(o => db.Branches.Where(b => b.SupervisorId == id).Select(b => b.Id).Contains(o.BranchId)).ToList();
            ICollection<OrderIndexViewModel> viewModel = new List<OrderIndexViewModel>();
            foreach(Order o in orders)
            {
                viewModel.Add(new OrderIndexViewModel(o, db.Users.Where(u => u.Id == o.UserId).FirstOrDefault(), db.Branches.Where(b => b.Id == o.BranchId).FirstOrDefault()));
            }
            return View("Index", viewModel);
        }

        // GET: Orders/MyOrders
        [RequireLoginFilter]
        public ActionResult MyOrders()
        {
            Guid id = (Session["USER"] as User).Id;
            ICollection<Order> orders = db.Orders.Where(o => o.UserId == id).ToList();
            ICollection<OrderIndexViewModel> viewModel = new List<OrderIndexViewModel>();
            foreach (Order o in orders)
            {
                viewModel.Add(new OrderIndexViewModel(o, db.Users.Where(u => u.Id == o.UserId).FirstOrDefault(), db.Branches.Where(b => b.Id == o.BranchId).FirstOrDefault()));
            }
            return View("Index", viewModel);
        }

        // GET: Orders/Edit/5
        [RequireLoginFilter(AdminRequired = true)]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            ViewBag.Branches = db.Branches.ToList();
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [RequireLoginFilter(AdminRequired = true)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,BranchId, UserId")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Branch");
            }

            order.ProductOrders = db.Orders.Find(order.Id).ProductOrders;
            ViewBag.Branches = db.Branches.ToList();
            return View(order);
        }

        // POST: Orders/Delete/5
        [RequireLoginFilter(AdminRequired = true)]
        [HttpPost]
        public ActionResult Delete(Guid orderId)
        {
            Order order = db.Orders.Find(orderId);
            db.Orders.Remove(order);
            db.SaveChanges();
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        // POST: Orders/DeleteItem/5
        [RequireLoginFilter(AdminRequired = true)]
        [HttpPost]
        public ActionResult DeleteItem(Guid? ProductId, Guid? OrderId)
        {
            ProductOrder productOrder = db.ProductOrders.Find(ProductId, OrderId);
            db.ProductOrders.Remove(productOrder);
            db.SaveChanges();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
