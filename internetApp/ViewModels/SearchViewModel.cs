﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace internetApp.ViewModels
{
    public class SearchViewModel
    {
        public string category { get; set; }
        public string color { get; set; }
        public string brand { get; set; }
    }
}