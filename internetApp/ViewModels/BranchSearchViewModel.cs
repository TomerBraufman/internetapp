﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace internetApp.ViewModels
{
    public class BranchSearchViewModel
    {
        public string Name { get; set; }
        public string Supervisor { get; set; }
    }
}