﻿using internetApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace internetApp.ViewModels
{
    public class CatalogViewModel
    {
        public ICollection<Product> allProducts { get; set; }
        public ICollection<Product> recommendedProducts { get; set; }

        public CatalogViewModel(ICollection<Product> allProducts, ICollection<Product> recommendedProducts)
        {
            this.allProducts = allProducts;
            this.recommendedProducts = recommendedProducts;
        }
    }
}