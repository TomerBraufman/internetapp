﻿using internetApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace internetApp.ViewModels
{
    public class OrderIndexViewModel
    {
        public Order order { get; set; }
        public User user { get; set; }
        public Branch branch { get; set; }
        public OrderIndexViewModel(Order order, User user, Branch branch)
        {
            this.branch = branch;
            this.order = order;
            this.user = user;
        }
    }
}