﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using internetApp.Models;

namespace internetApp
{
    public class Context : DbContext
    {

        public Context() : base("name=ClothesStoreContext")
        {
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<ProductOrder> ProductOrders { get; set; }

        public DbSet<Branch> Branches { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}