﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace internetApp.Models
{
    public class ProductOrder
    {
        [Key, ForeignKey("Product"), Column(Order = 0)]
        public Guid ProductId { get; set; }

        [Key, ForeignKey("Order"), Column(Order = 1)]
        public Guid OrderId { get; set; }

        public int Quantity { get; set; }

        public virtual Product Product { get; set; }
        public virtual Order Order { get; set; }
    }
}