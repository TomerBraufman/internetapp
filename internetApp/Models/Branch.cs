﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.ComponentModel;

namespace internetApp.Models
{
    public class Branch : Entity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public double Longtitude { get; set; }
        [Required]
        public double Latitude { get; set; }
        [Required, JsonIgnore]
        public virtual Guid SupervisorId { get; set; }
        [ForeignKey("SupervisorId"), JsonIgnore]
        public virtual User Supervisor { get; set; }
    }
}