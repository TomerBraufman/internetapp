﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace internetApp.Models
{
    public class User : Entity
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [ForeignKey("UserId")]
        public virtual ICollection<Order> Orders { get; set; }
        [Required]
        public string FullName { get; set; }
        public string Address { get; set; }
        [Required]
        public bool IsAdmin { get; set; }
    }
}