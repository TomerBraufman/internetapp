﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace internetApp.Models
{
    public class Product : Entity
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public string Category { get; set; }
        [Required]
        public string Color { get; set; }
        [Required]
        public string Brand { get; set; }
        [Required]
        public int Price { get; set; }
        [Required]
        public string Picture { get; set; }
        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
    }
}