﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace internetApp.Models
{
    public class Order : Entity
    {
        [Required]
        public Guid UserId { get; set; }
        [Required]
        public Guid BranchId { get; set; }
        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
    }
}