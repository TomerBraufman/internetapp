﻿var map;
var marker;
$.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyBjm3M7bvE0Kzy3kNk_pwba_LkRyICqgdI", function () {
    initMap();
});
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: center.lat, lng: center.lng },
        zoom: 8
    });
    if (center.toTuggle) {
        placeMarker(new google.maps.LatLng({ lat: center.lat, lng: center.lng }))
    }

    map.addListener('click', function (e) {
        placeMarkerAndPanTo(e.latLng);
    });

    checkform();
}

function removeMarker() {
    marker.setMap(null);
    document.getElementById('Latitude').value = null;
    document.getElementById('Longtitude').value = null;
    checkform();
}

function placeMarkerAndPanTo(latLng) {
    placeMarker(latLng)
    document.getElementById('Latitude').value = latLng.lat();
    document.getElementById('Longtitude').value = latLng.lng();
    checkform();
    map.panTo(latLng);
}

function placeMarker(latLng) {
    if (marker !== undefined) {
        marker.setMap(null);
    }
    marker = new google.maps.Marker({
        position: latLng,
        map: map
    });
    marker.addListener('click', removeMarker)
}

function checkform() {
    var cansubmit = true;
    if (document.getElementById('Latitude').value === "" ||
        document.getElementById('Longtitude').value === "") {
        cansubmit = false;
    }

    document.getElementById('submit-creation').disabled = !cansubmit;
}
