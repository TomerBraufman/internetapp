﻿function AddToCart(id) {
    $.ajax({
        type: "post",
        url: "/ShoppingCart/Add",
        data: { prodId: id },
        dataType: "application/json"
    });
}